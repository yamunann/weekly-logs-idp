# Weekly Logs for IDP

**NAME          : YAMUNAN A/L SELVANATHAN**

**MATRIC NUMBER : 199091**

**SUBSYSTEM : PAYLOAD DESIGN & SPRAYER**

---

**CHECKLIST**
- [x] Week 3
- [x] Week 4
- [x] Week 5
- [x] Week 6
- [x] Week 7
- [ ] Week 8
- [ ] Week 9
- [ ] Week 10
- [ ] Week 11

---

|ENTRY NUMBER|TIME(HRS)|DATE|AGENDA|GOALS|IMPORTANT DECISIONS MADE|JUSTIFICATION|IMPACT ON PROJECT|NEXT STEP|
| :----------: | :--: |:-----------------------:| :----: | :---: | :----------------------: | :-----------: | :---------------: | :-------: |
|03|14:24|5/11/2021|Submission of Gitlab tasks 1,2 & division of groups,subsystems|To submit the tasks on time and to know our respective subsytems and groups for IDP|Decided to play around with Gitlab so that will be helpful for logging and documenting our progress on our project in the following weeks|The weekly logs should be documented in Gitlab thus familiarizing ourselves with Gitlab will be extremely helpful|We got to know on what subsystems we will be working in|To gather information about agriculture drones|
|04|17:55|11/11/2021|Site visit to get more information on commercial agriculture drones (eg. DJI Agras)|To get more info on commercially available agriculture drones|Decided to have a look at the industry leading agriculture drones, the DJI Agras T20 and T16|They are one of the best agriculture drones available in the market and having a look at them in real life will help us to actually understand the design and the engineering behind a sprayer system|We got to know the various parts in agriculture drones|To decide on what our sprayer will consist of|
|05|20:44|18/11/2021|Meeting with Mr.Fiqri at the Lab H2.1|Proposal of parts, the design and basic calculations to come up with sprayer system|To finalize the design and decide the main components that will be used to build the sprayer|We weighed a few options and made the choice to proceed with the chosen ones after discussing with Mr.Fiqri|We looked at both the pros and cons of the options and chose the most efficient ones bearing in mind the cost|We could proceed with the final design of our sprayer as we have already decided on the parts that the sprayer will consist of|To find the products availability via online (eg. Shopee, Aliexpress, Lazada, and few retailers)|       
|06|23:30|25/11/2021|Calculations, Finalizing the tank to hold the pesticide (the volume, shape etc)|To finalize the deisgn of the whole sprayer system including the tank as well as caculating the volume of pesticide needed with respect to the filed speed of the HAU|We decided to proceed with the 5L tank and came up with a few calculations to find out the Gallons per Acre, Gallons per Minute and the flow rate of the nozzles|5L tank was the best to go with since anything more than that would be too heavy for a payload nad anything less than that would be too little as frequent refilling would be needed|We got to proceed with purchasing|To test out the nozzles so that we could decide on what would be the most suitable one with respect to the field speed as well as the pesticide flow rate needed |       
|07|12:34|3/12/2021|Purchasing stuffs needed from HArdware shop as well as Shopee|To buy the connectors, PVC pipes and cable tie that will be needed for the sprayer system|We tried our best to cut the cost by purchasing items that are not very expensive but can cater what we need|The whole idea of this project was to come up with a HAU at a very minimal cost thus it was important to cut down cost as much as possible|The order that we placed on Shopee for the 5L tank and the things that we bought from the hardware shop allow us to proceed with the assembly of the sprayer system maybe from week 8 onwards|To assemble the sprayer system with the things we already got/bought|        

